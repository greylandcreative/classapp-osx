//
//  Student.m
//  ClassApp
//
//  Created by Corey Kahler on 6/2/16.
//  Copyright (c) 2016 Corey Kahler. All rights reserved.
//

#import "Student.h"

@implementation Student


- (void) displayRole {
    NSLog(@"Student");
}


- (NSString*) getFullName:(NSString*)first secondName:(NSString*)second {
    NSString* full = [ first stringByAppendingString: second];
    return full;
}
    

@end
