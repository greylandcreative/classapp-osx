//
//  ViewController.h
//  ClassApp
//
//  Created by Corey Kahler on 6/2/16.
//  Copyright (c) 2016 Corey Kahler. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ViewController : NSViewController<NSTableViewDataSource, NSTableViewDelegate>

- (void) createStudent;

@property (weak) IBOutlet NSTextField *firstName;
@property (weak) IBOutlet NSTextField *secondName;
@property (weak) IBOutlet NSTextField *total;

@property NSMutableArray *students;
@property (weak) IBOutlet NSTableView *studentOutput;

- (IBAction)toolTip:(id)sender;

@end
