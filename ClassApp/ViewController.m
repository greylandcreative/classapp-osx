//
//  ViewController.m
//  ClassApp
//
//  Created by Corey Kahler on 6/2/16.
//  Copyright (c) 2016 Corey Kahler. All rights reserved.
//

#import "ViewController.h"
#import "Student.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.students = [[NSMutableArray alloc] init];
    
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (void)createStudent {
    NSLog(@"Creating student");
    Student *newStudent = [[Student alloc] init];
    
    newStudent.firstName = self.firstName.stringValue;
    newStudent.lastName = self.secondName.stringValue;
    [self.students addObject:newStudent];
    
    NSString *newCount = [NSString stringWithFormat:@"%lu", (unsigned long)[self.students count]];
    newCount = [@"Total: " stringByAppendingString: newCount];
    
    [self.total setStringValue: newCount];
}

- (IBAction)createClicked:(id)sender {
    [self createStudent];
    [self.firstName setStringValue:@""];
    [self.secondName setStringValue:@""];
    [self.studentOutput reloadData];
}

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    return self.students.count;
}

- (NSView *)tableView:(NSTableView *)tableView
   viewForTableColumn:(NSTableColumn *)tableColumn
                  row:(NSInteger)row {
    
    // The return value is typed as (id) because it will return a string in most cases.
    NSString *cellIdentifier;
    NSString *text;
    
    // Student
    Student *stud = [self.students objectAtIndex:row];
    
    // The column identifier string is the easiest way to identify a table column.
    NSString *columnIdentifer = [tableColumn identifier];
    
    // Compare each column identifier and set the return value to
    // the Person field value appropriate for the column.
    if ([columnIdentifer isEqualToString:@"firstName"]) {
        text = stud.firstName;
        cellIdentifier = @"NameCellID";
    } else {
        text = stud.lastName;
        cellIdentifier = @"LastNameCellID";
    }
    
    NSTableCellView *cell = [tableView makeViewWithIdentifier:cellIdentifier owner:self];
    cell.textField.stringValue = text;
    
    return cell;
}


- (IBAction)toolTip:(id)sender {
    
}

@end
