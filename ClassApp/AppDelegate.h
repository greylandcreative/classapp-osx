//
//  AppDelegate.h
//  ClassApp
//
//  Created by Corey Kahler on 6/2/16.
//  Copyright (c) 2016 Corey Kahler. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

