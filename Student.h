//
//  Student.h
//  ClassApp
//
//  Created by Corey Kahler on 6/2/16.
//  Copyright (c) 2016 Corey Kahler. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Student : NSObject

@property NSString *firstName;
@property NSString *lastName;

- (void) displayRole;
- (NSString*) getFullName:(NSString*)first secondName:(NSString*)second;

@end
